export const API_HOST = "http://127.0.0.1";
export const API_URL = API_HOST + '/index.php/api';
export const VERSION = "1.0.0";

/**
 * 相关api接口地址
 * @type {{...}}
 */
export const urls = {
	member: {
		login: API_URL + '/member/wxlogin',
		info: API_URL + '/member/getInfo',
		save: API_URL + '/member/setInfo',
	},
	public: {
		upload: API_URL + '/utils/upload',
		feedback: API_URL + '/logs/feedback'
	}
};